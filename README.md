# CSE-6705_Group01

Project details of CSE 6705 : Meta-heuristics (April 2018).
Group - 01


Downloaded/ Used Tools:
1. Quartetnet: http://sysbio.cvm.msstate.edu/QuartetNet/  ==> Purpose [Quartet to tree calculation]
2. Quartet Decomposition Server: http://quartets.uga.edu  ==> Purpose [Tree to quartet decomposition server]
   
NB: Quartet Decomposition Server is managed by the authors. The service is often found unstable.


3. ASTRID: https://github.com/pranjalv123/ASTRID          ==> ASTRID is used to calculate species trees from a set ofgene trees.
4. ASTRAL: https://github.com/smirarab/ASTRAL             ==> ASTRAL is used to calculate species trees from a set ofgene trees.
5. MRP: https://github.com/smirarab/mrpmatrix             ==> MRP is used to calculate species trees from a set ofgene trees.


Download and compile the tools (if necessary) as per instructions provided in the official documents.

Commands:
ASTRAL: java -jar astral.jar -i "input Gene Trees" -o "Output Species Tree"

ASTRID: ./ASTRID-linux -i "input Gene Trees" -o "Output Species Tree"

MRP: ./runMRP.py -i "input Gene Trees" -o "Output Species Tree" -l mylog -n 100 -d "Workspace DIR"

Thirdparty Tool Usage: 


Quartet-Net run command: https://gitlab.com/dip06ece/cse-6705_group01/blob/master/DOC/UserManual.pdf


Stripping edge support: ./strip_edge_support2.pl -i ""Input Tree"" -o "Output Stripped Tree"


Quartet decomposition server: Detailed instructions and sample data set are available in http://quartets.uga.edu.


If you have further queries, please contact.
Email: ranjon_dip@yahoo.com
       lingkon002@yahoo.com
