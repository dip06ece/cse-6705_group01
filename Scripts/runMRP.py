#!/usr/bin/env python

###########################################################################
##    Copyright 2010 Rahul Suri and Tandy Warnow.
##    This file is part of ReUP.
##
##    ReUP is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    ReUP is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with ReUP.  If not, see <http://www.gnu.org/licenses/>.
###########################################################################

import sys
import os
import getopt
import dendropy
import re
import math
#import HTML
import random, tempfile

from optparse import OptionParser, OptionGroup
from reup.reup import supplyReup

#from dendropy.dataio import trees_from_newick
#from dendropy.scripts.strict_consensus_merge import strict_consensus_merge
from newick_modified.tree import parse_tree
from spruce.mrp import *
print "Line 37"
def main(argv):	
	inputFile = ""
	outputFile = ""	
	iters = 100
	logFile = ""
	best = None
	directory = None
	print "Line 45"
    	try:
        	opts, args = getopt.getopt(argv, "i:o:l:n:b:d:", ["input=", "output=", "log=", "iter=", "best=", "dir="])
    	except getopt.GetoptError:
		print "Error!"
        	#usage()
        	sys.exit(2)	
	for opt, arg in opts:
		if opt in ("-i", "--input"):
			inputFile = arg
		elif opt in ("-o", "--output"):
			outputFile = arg
		elif opt in ("-l", "--log"):
			logFile = arg
		elif opt in ("-n", "--iter"):
			iters = int(arg)
		elif opt in ("-b", "--best"):
			best = arg
		elif opt in ("-d", "--dir"):
			directory = arg
	print "Line 65"		
	if (logFile == ""):		
		log = sys.stderr
	else:
		log = open(logFile,'w')
	if (directory is not None):
		tempfile.tempdir = directory
	
        f = tempfile.NamedTemporaryFile()
        prefix = f.name
	f.close()
	print "Line 76"
	path = os.getenv("FASTMRP")	
	pipe = Popen("java -jar %s/mrp.jar %s %s.mrp NEXUS " % (path, inputFile, prefix), shell = True, stdout = PIPE, stderr = PIPE)
        (out, err) = pipe.communicate()
	print "Line 80"
	#Grab the number of taxa/columns from the mrp file
	numTaxa = None
	numColumns = None
	mrpFile = open(prefix+".mrp", 'r')
	print "Line 85"
	for line in mrpFile:
		regex = re.compile("dimensions\s+ntax\s*=\s*(\d+)\s+nchar\s*=\s*(\d+)")
		r = regex.search(line)
		if (r is None):
			continue
		else:
			(numTaxa, numColumns) = (int(r.groups()[0]), int(r.groups()[1]))
			break
	print "Line 94"	
	#Append the commands to run ratchet search at end of nexus file
        f = open (prefix+".mrp", 'a')
        writeRatchetInputFile(None, f, filePrefix = prefix, numRatchetIterations = 100, numChars = numColumns )
        f.close()
	print "Line 99"
        pipe = Popen("paup -n %s.mrp" % prefix, shell = True, stdout = PIPE, stderr = PIPE)
        (out, err) = pipe.communicate()
	print "Line 102"
	trees = None
	bestTrees = None
	trees = getConsensusTreesFromPaupFiles(prefix)
	if (best):
		mpTrees = readTreesFromRatchet(prefix+".tre.best")
		bestTrees = random.choice(mpTrees)
		print "Line 109"
	try:
		os.remove(prefix + ".log")
		os.remove(prefix + ".gmrp")
		os.remove(prefix + ".smrp")
		os.remove(prefix + ".mmrp")
		os.remove(prefix + ".tre")
		os.remove(prefix + ".tre.nex")
		os.remove(prefix + ".mrp")
		os.remove(prefix + ".tre.best")
		print "Line 119"
	except:
		print "Error removing files\n"
		print "Line 122"		
				
	#print "My Dir: " + curDir
	
	output = trees['gmrp']		
	out = open(outputFile,"w")
	out.write(str(output))
	out.flush()
	out.close()
	print "Line 131"
	if (best):
		out = open(best,"w")
		out.write(str(bestTrees))
		out.flush()
		out.close()
		print "Line 137"
if __name__ == "__main__":
	main(sys.argv[1:])
